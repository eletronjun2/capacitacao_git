#include <Arduino.h>
#include <esp_sleep.h>

float temperatura;
float tensao_bateria;

void setup() {
  Serial.begin(115200);
}

void loop() {
  Serial.println(temperatura);
  Serial.println(tensao_bateria);
}

void sleep_mode() {
  esp_sleep_enable_timer_wakeup(5000000);

  esp_deep_sleep_start();
}