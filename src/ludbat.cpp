#include <Arduino.h>
#include "ludbat.h"

float tensaobat(int pino){
    pinMode(pino, INPUT);
    tensao = analogRead(pino);
    return tensao;
}

float porcentagembat (int pino){
    pinMode(pino, INPUT);
    float tensao = analogRead(pino);
    float porcentagem = map(tensao, 0.0f, 4095.0f, 0, 100);
    return porcentagem;
}

float porcentagembat_semmap (int pino, float tensao_max){
    pinMode(pino, INPUT);
    float tensao1 = analogRead(pino);
    float tensao2 = (tensao1/4095.0)*2*1.1*3.3;
    float porcentagem = tensao2/tensao_max;
    return porcentagem;  
}