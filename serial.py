import serial as pyserial
import matplotlib.pyplot as plt
import numpy as np

# vetor fictício para simular a leitura da serial
valores = ["12.5\n", "24.3\n", "13.8\n", "28.1\n", "15.2\n", "25.7\n", "11.9\n", "23.4\n", "12.5\n", "24.3\n", "13.8\n", "28.1\n", "15.2\n", "25.7\n", "11.9\n", "23.4\n"]

# configuração serial
ser = pyserial.Serial(port='COM5', baudrate=115200)

# vetor para armazenar valores lidos da serial
temperatura = np.zeros(8)
bateria = np.zeros(8)

# leitura dos valores da serial e armazenamento nos vetores de temperatura e bateria
contador = 0
for i in range(16):
    vetor_value = valores[i]
    try:
        value = float(vetor_value)
        if contador % 2 == 0:
            temperatura[contador // 2] = value
        else:
            bateria[contador // 2] = value
        contador += 1
    except ValueError:
        pass

# Plota o gráfico dos dados temperatura
plt.figure(1)
plt.title("Dados temperatura")
plt.plot(temperatura)

# Plota o gráfico dos dados bateria
plt.figure(2)
plt.title("Dados bateria")
plt.plot(bateria)

plt.show()